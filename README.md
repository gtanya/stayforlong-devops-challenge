# Stayforlong Challenge

This repository includes the definition, the design and the implementation of a DevOps technical challenge based on the Stayforlong's [requirements](doc/requirements.md).

## Infrastructure solution

The following diagram shows the designed infrastructure that you could
deploy with this repository. This design **offers a cross environment
solution independent of the kind of workload to execute**.

![aws diagram img](/doc/img/aws_infrastructure.png)

There's a base infraestructure, independent from the environment:

* **VPC**
* **Public subnets**
* **Private subnets**
* **Internet Gateway**

There's an environment infrastructure composed by:

* **Application Load Balancer** (ALB), which publish the http
  connection, and the only opened port to instances by default.
* **ECS Cluster**, with
    * **EC2 instances** with docker. They execute the workloads (GO application & others). Placed into private zone for security reasons.
    * **EC2 autoscaling group**
* **Bastion Host**, an EC2 instance to access with SSH to the EC2
  instances.
    
The motivation about this solution is detailed at
[/doc/techincal_decision](/doc/techincal_decisions.md).

## Infrastructure as Code design

Script

Ansible

CloudFormation

## About the GO application

The GO application repository has been modified to make possible to
build de docker image. It has been created a cloned repository with the
changes at https://gitlab.com/gtanya/stayforlong-go-hello-world

A docker image has been upload at
https://hub.docker.com/r/gtanya/stayforlong-go-hello-world to be used in
this challenge.

## Installation

Check the documentation [doc/installation.md](/doc/installation.md).


## Customization

Check the documentation [doc/customization.md](/doc/customization.md)
for customization your environments, environment parameters or
workloads.