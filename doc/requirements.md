# DevOps challenge

We'd like you to submit your exercise as a Git repository so that we can see how you elaborated on your solution.

You have a week to complete it so please take your time. There are no extra points for doing it quickly. We would prefer that you take your time and deliver a solution that you're proud of.

## Problem definition

Build and deploy an application to AWS. The source code of the application is available at: https://github.com/stayforlong/go-hello-world

The application will be accessible only from a load balancer, the load balancer will be publicly accessible in default HTTP port and the instance will be accessible through SSH only from a management security group with defined CIDRs.

The solution must be designed with high availability in mind, an instance not passing the health checks must be automatically replaced.

Here are some guidelines on how to approach the problem and what to deliver:

### Requirements

- Include a README.md file explaining ​ at least ​ how to install and run the solution.
- Use Infrastructure as Code to accomplish the task (Ansible, Terraform, Cloudformation, custom code...or a mix of). Manual actions are not allowed.
- The complete application stack must be repeatable from scratch in different
environments (development, staging, production).

### Nice to have

- No previous resources in the AWS account are required besides the user with the required permissions to run the solution.
- Allow customization of the values that may vary when running in different environments or for different applications
- Reduce to the minimum the time required by new instances to be ready to serve traffic (when scaling up or replacing failing ones)
- Having some scaling based on CPU will be appreciated.

### Notes

- Use the technology stack you feel more comfortable with.
- Assume Docker will be available in the machine running your solution.
- If you choose to write the solution using any scripting language, consider writing good code.
- Don’t bring a single commit solution. Commit as often as possible while providing some value or functionality in each commit.