# Technical design decisions

**TODO** ToC

## Presumptions

* The application doesn't need Internet connection. On the other hand, it should be necessary to have an AWS NatGateway.
* It's considered to give a minimum security setting up the public and
  private subnets. This avoid direct access to resources.
* Only the ALB is on private subnet.
* By an arbitrary decision to make simplest the solution, the VPN and
  his subnets resources are shared across the different environments.

## Autoscaling

Autoscaling can be from 2 point of view:

* **ECS service tasks**. Application services with high load can scale out
  automatically and scale in when the load decrease. 
* **EC2 instances**. As the application load can be increased and they can
  be scaled, EC2 base infrastructure can scale out/in automatically when
  exceed the fixed threshold.

## High Availability

High Availability (HA) is the main reason which is based the design. The
solution has enough resilience to infrastructure and application
failures.

The approach has the following components that an error on it could
produce a service failure.

* **AWS AZ**. It's not a common situation, but it could happen. It's a
  good practice to work with at least 2 AZ.
* **Application instance**. 
* **AWS EC2 instance**.
* **Load Balancer**. Not covered.

A Load Balancer failure isn't covered due to the challenge scope. It's
an arbitrary decision to lean on native AWS HA infrastructure on ELB's. 

## (Non) Shared AWS infrastructure discussion

AWS VPC and his subnets are shared across environments. It could be
interesting to have different subnets per environment to isolate traffic
and access between them.

On this solution, for simplicity, subnets are shared and it could be
traffic between environments.

## Build & Delivery scope

This solution covers the build and the delivery from the local machine. 

However, should be recommended to have CI/CD for the repository in order
to automate tasks and to have a better testing process.

## Bastion Host

EC2 instaces used with the ECS cluster are placed into the private
subnets to prevent direct access. 

For this reason, to access through ssh to them it's needed a Bastion
Host.

## Requirements justification

> The application will be accessible only from a load balancer, the load balancer will be publicly accessible in default HTTP port

**TODO**

> the instance will be accessible through SSH only from a management
> security group with defined CIDRs.

**TODO**

> The solution must be designed with high availability in mind, 

**TODO**

> an instance not passing the health checks must be automatically
> replaced.

**TODO**

> Include a README.md file explaining ​ at least ​ how to install and run
> the solution. 

**TODO**

> Use Infrastructure as Code to accomplish the task (Ansible, Terraform,
> Cloudformation, custom code...or a mix of). 

**TODO**

> Manual actions are not allowed.

**TODO**

> The complete application stack must be repeatable from scratch in
> different environments (development, staging, production).

**TODO**

> No previous resources in the AWS account are required besides the user
> with the required permissions to run the solution.
   
**TODO**

> Allow customization of the values that may vary when running in
> different environments or for different applications

**TODO**

> Reduce to the minimum the time required by new instances to be ready
> to serve traffic (when scaling up or replacing failing ones)

**TODO**

> Having some scaling based on CPU will be appreciated.

**TODO**

## Bonus points 

* Shared EC2 instances, aproifitar recursos
* Different subnets, contrari a l'anterior per separar completament.
