# Customization

* [Customize ECS cluster](#customize-ecs-cluster)
* [Customize workload settings](#customize-workload-settings)
* [Add workload to an environment](#add-workload-to-an-environment)
* [Add new environment](#add-new-environment)




## Customize ECS cluster

You can edit your ECS cluster parameters for scaling out/in your
infraestructure. Take the example code:

```
    - role: ecs
      vars:
        stack_name: 'sfl-ecs-development'
        network_stack_name: 'sfl-network'
        desired_capacity: '2'
        max_size: '4'
        instance_type: 't2.small'
        pairkey_name: 'development-pk'
        state: present
```

Where:
* **desired_capacity** is the minimum EC2 instances running at time
* **max_size** the maximum EC2 instances that can be runnnig
* **instance_type** the EC2 type for all cluster instances


## Customize workload settings

You can edit your Workloads scaling out/in your hi containers across ECS
instances. Take the example code:

```
    - role: workload
      vars:
        stack_name: 'sfl-dev-app-go'
        ecs_stack_name: 'sfl-ecs-development'
        service_name: 'dev-app-go'
        image_url: 'gtanya/stayforlong-go-hello-world'
        container_port: '80'
        container_cpu: '256'
        container_memory: '256'
        path: '*'
        desired_count: '3'
        priority: 10
        health_path: "/health"
        state: present
```

Where:
* **container_port** is the running container port which LoadBalancer
  will point. This not corresponds to Internet published port
* **container_cpu** the maximum CPU used by the container
* **container_memory** the maximum RAM Memory used by the container
* **desired_count** the number of container instances tot set up
* **priority** the LoadBalancer rule priority for the "path"

## Add workload to an environment

To add a workload in an existing environment, you should edit the
environment playbook file with:

**Note**: replace "dev" or "development" with your environment.

```
    - role: workload
      vars:
        stack_name: 'sfl-dev-app-<WORKLOAD_ID_NAME>'
        ecs_stack_name: 'sfl-ecs-development'
        service_name: 'dev-app-<WORKLOAD_ID_NAME>'
        image_url: '<DOCKERHUB_IMAGE>'
        container_port: '80'
        container_cpu: '256'
        container_memory: '256'
        path: '/<URL_PATH>'
        desired_count: '3'
        priority: 1
        health_path: "/<HEALTH_PATH>"
        state: present
```

Where:
* **WORKLOAD_ID_NAME** should be the unique name identifier about your
  environment
* **DOCKERHUB_IMAGE** should be the name of the docker image to run.
* **URL_PATH** should be the unique URL path which the workload will be
  accessible from Internet.
* **HEALTH_PATH** should be the path, if exists, which the workload can
  validate his health
  
The other parameters can be modified according to the needs.

## Add new environment

To add a new environment it could be done replicating, renaming and
editing the playbooks
[/ansible/playbooks/development-up.yaml](/ansible/playbooks/development-up.yaml)
and
[/ansible/playbooks/development-down.yaml](/ansible/playbooks/development-down.yaml)

You need to change:
* All references to "development" literal
* All references to "dev" literal

If you want a Bastion Host for other environment, you should replicate,
rename and edit the playbooks
[/ansible/playbooks/bastion-dev-up.yaml](/ansible/playbooks/bastion-dev-up.yaml)
and
[/ansible/playbooks/bastion-dev-down.yaml](/ansible/playbooks/bastion-dev-down.yaml)




