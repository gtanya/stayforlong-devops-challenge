# Installation

This documentation explains how to use this stack.

* [Requirements](#requirements)
* [Clone Repository](#clone-repository)
* [Deploy](#deploy)
  * [Deploy The Common Network](#deploy-the-common-network)
  * [Deploy an environment](#deploy-an-environment)
  * [Deploy a Bastion Host](#deploy-a-bastion-host)
* [Clean Up](#clean-up)

## Requirements

This stack has the following requirements:

* It works under linux host
* It could work on windows, but the steps will be different (not
  covered)
* Bash terminal is recommended. No tests with others.
* ssh-agent installed to SSH connections and Key management.
* It needs an AWS user and his Key ID and Secret Access Key. Check [the 
       guide](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html#Using_CreateAccessKey)
  to obtain it.
* The AWS CLI installed on local. Check
  [installation](https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html)
  guide.
* The user credentials configured for the AWS CLI with `aws configure`
* Docker installed on host

## Clone repository

Execute the following to clone the stack repository: 

```
git clone https://gitlab.com/gtanya/stayforlong-devops-challenge.git
```


## Deploy

### Deploy the common Network

The following will deploy a VPC with his subnets. It will be used across
environments:

```
./run-playbook.sh ansible/playbooks/network-up.yaml
```

You can assure on AWS Console that the CF template has been deployed:

![cf network stack console](/doc/img/cf_network.png)

### Deploy an environment


The following will deploy the ECS environemtn stack with his workloads.
Also it will create the SSH Key used to connect instances and it'll be
installed in you ssh-agent keyring.

```
./run-playbook.sh ansible/playbooks/development-up.yaml
```

Check the console result to get the domain enabled to access the apps:


![ansible ecs stack output](/doc/img/ansible_ecs_result.png)


Also you can review on AWS CloudFoundry page 

![cf ecs stack console](/doc/img/cf_ecs.png)


### Deploy a Bastion host

Optionally, you can deploy a Bastion Host to access EC2 instance through
SSH connection with:

```
./run-playbook.sh ansible/playbooks/bastion-dev-up.yaml
```

Check the console to get ssh command with ip:

![ansible bastion host output](/doc/img/ansible_bastion_host_result.png)


Or get from AWS CloudFormation

![cf bastion host stack console](/doc/img/cf_bastionhost.png)

## Clean Up

**Importan!**: the order of the steps is mandatory.


If you have deployed, delete the Bastion Host:

```
./run-playbook.sh ansible/playbooks/bastion-dev-down.yaml
```

Then, delete all environments you deployed

```
./run-playbook.sh ansible/playbooks/development-down.yaml
```

Last, delete network

```
./run-playbook.sh ansible/playbooks/network-down.yaml
```