#!/bin/bash

####################################################
# Script: run-playbook.sh
# Maintainer: guillemtanya@gmail.com
# Description:
#    This scripts executes an ansible playbook
#    passed by parameter with the docker image
#    'cytopia/ansible' and the flavor 'latest-aws'
#    to get AWS capabilities.
####################################################

shopt -s expand_aliases # Enable aliases usage in the script


#################      Checks        #################

# It must be passed the path parameter
if [[ -z $1 ]]; then
  echo "Run help:  ./$0 <path_to_ansible_playbook>"
  exit 1
fi



##################      MAIN        ##################

## Get user machine for docker image
user_id=$(id -u)
group_id=$(id -g)

## Create alias shortcut
alias ansdock="docker run --rm --volume $SSH_AUTH_SOCK:/ssh-agent --env SSH_AUTH_SOCK=/ssh-agent -e USER=ansible -e MY_UID=${user_id} -e MY_GID=${group_id} -e AWS_PROFILE="gtp" -v ${HOME}/.aws/config:/home/ansible/.aws/config:ro -v ${HOME}/.aws/credentials:/home/ansible/.aws/credentials:ro -v $(pwd):/data cytopia/ansible:latest-aws"


## Execute the playbook
ansdock ansible-playbook $1
